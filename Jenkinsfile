pipeline {
	agent any

	options {
	    disableConcurrentBuilds()
	    buildDiscarder(logRotator(numToKeepStr: '10'))
	    ansiColor(colorMapName: 'XTerm')
	}

	triggers {
		bitbucketPush()
	}

	tools {
		jdk 'jdk8'
		maven 'mvn-3.5.2'
	}

	parameters {
    booleanParam(defaultValue: true, description: 'Run tests with or without SonarQube', name: 'runSonarQube')
  }

	stages {
		stage('Build') {
			steps {
				sh 'mvn clean package -DskipTests'
				archiveArtifacts artifacts: '**/target/*.jar', fingerprint: true
			}
		}

		stage('Test - Unit - API - Performance - GUI') {
			steps {
				sh 'mvn verify'
				junit '**/target/surefire-reports/*.xml'
				gatlingArchive()
			}
			post {
				always {
					cucumber buildStatus:  'UNSTABLE',
																	fileIncludePattern: '**/cucumber.json',
																	trendsLimit: 10
				}
			}
		}

		stage('report to Sonar') {
			when {
				expression {
					params.runSonarQube == true
				}
			}
			steps {
				sh 'mvn sonar:sonar \
              -Dsonar.projectKey=avansprojects_test-automatisering \
              -Dsonar.organization=avansprojects \
              -Dsonar.host.url=https://sonarcloud.io \
              -Dsonar.login=f3a94dd20766bf2d5c8cdb816935021dc3468d63'
			}
		}

		stage('NeoLoad') {
			steps {
				echo 'NeoLoad pipeline as code is broken, neoloadRun parameters do not match the parameters required by NeoLoadCmd'
				echo 'So, we have another job that runs the NeoLoad tests that is triggered by this build'
				build 'neoload'
//				neoloadRun(scenarioName: 'GetPaymentScenario', scenario: 'GetPaymentScenario', customCommandLineOptions: '-debug', displayGUI: false, executable: 'NeoLoadCmd', localProjectFile: '/var/lib/jenkins/neoload-projects/neoload/Avans.nlp', showTrendAverageResponse: true, showTrendErrorRate: true, autoArchive: true)
//				neoloadRefreshTrends(showTrendAverageResponse: true, showTrendErrorRate: true)
			}
		}
	}
}
