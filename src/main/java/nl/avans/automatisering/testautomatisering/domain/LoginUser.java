package nl.avans.automatisering.testautomatisering.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginUser {
    @NotBlank
    @Size(min = 2, max = 20, message = "username must be between 2 and 20 characters")
    private String username;
    @NotBlank
    @Size(min = 6, max = 200, message = "password must be between 6 and 200 characters")
    private String password;
}
