package nl.avans.automatisering.testautomatisering.service;

import java.util.ArrayList;
import java.util.List;

import nl.avans.automatisering.testautomatisering.domain.LoginUser;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class LoginService {
    private List<LoginUser> users = new ArrayList<>();

    public boolean login(LoginUser user) {
        LoginUser foundUser = null;
        for (LoginUser listed : this.users) {
            if (listed.getUsername().equals(user.getUsername())) {
                foundUser = listed;
            }
        }
        if (foundUser != null) {
            if (!user.getPassword().equals(foundUser.getPassword())) {
                throw new IllegalArgumentException("passwords do not match");
            }
            return true;
        } else {
            return false;
        }
    }

    public void setUsers(List<LoginUser> users) {
        this.users = users;
    }
}
