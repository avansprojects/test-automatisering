package nl.avans.automatisering.testautomatisering.unit;

public class ClassAssignmentAvans {
    public double shippingCosts(final boolean calculateShippingCosts, final String typeOfShippingCosts, final double totalPrice) {
        double result;
        if (calculateShippingCosts) {
            if (totalPrice > 1500) {
                result = 0;
            } else {
                switch (typeOfShippingCosts) {
                    case "Ground":
                        result = 100;
                        break;
                    case "InStore":
                        result = 50;
                        break;
                    case "NextDayAir":
                        result = 250;
                        break;
                    case "SecondDayAir":
                        result = 125;
                        break;
                    default:
                        result = 0;
                        break;
                }
            }
        } else {
            result = 0;
        }
        return result;
    }
}