package nl.avans.automatisering.testautomatisering.web;

import javax.validation.Valid;

import nl.avans.automatisering.testautomatisering.domain.LoginUser;
import nl.avans.automatisering.testautomatisering.domain.Result;
import nl.avans.automatisering.testautomatisering.service.LoginService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class LoginController {

    private final LoginService service;

    @PostMapping(value = "/login", produces = "application/json")
    public ResponseEntity<Result> login(@RequestBody @Valid LoginUser user) {
        Result.ResultBuilder builder = Result.builder()
            .username(user.getUsername());
        if (service.login(user)) {
            builder
                .message("login successful")
                .success(true);
            return ResponseEntity.ok(builder.build());
        } else {
            builder
                .message("login failed")
                .success(false);
            return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(builder.build());
        }
    }
}
