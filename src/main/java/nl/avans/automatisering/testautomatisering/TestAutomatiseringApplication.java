package nl.avans.automatisering.testautomatisering;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestAutomatiseringApplication {

    public static void main(String... args) {
        SpringApplication.run(TestAutomatiseringApplication.class, args);
    }

}
