package nl.avans.automatisering.testautomatisering;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestAutomatiseringApplicationTests {

    @Test
    public void contextLoads() {
        // a simple empty method to make sure the application CAN start up
    }

}
