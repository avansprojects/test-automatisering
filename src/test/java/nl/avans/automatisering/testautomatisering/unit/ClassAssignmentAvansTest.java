package nl.avans.automatisering.testautomatisering.unit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ClassAssignmentAvansTest {
    private static final String GROUND = "Ground";
    private static final String IN_STORE = "InStore";
    private static final String NEXT_DAY_AIR = "NextDayAir";
    private static final String SECOND_DAY_AIR = "SecondDayAir";
    private static final String UNKNOWN = "Unknown";

    private ClassAssignmentAvans sut;


    @Before
    public void setUp() {
        sut = new ClassAssignmentAvans();
    }

    @Test
    public void calculateShippingCostsFalse() {
        double result = sut.shippingCosts(false, GROUND, 100);
        assertEquals(0, result);
    }

    @Test
    public void priceOver1500() {
        double result = sut.shippingCosts(true, GROUND, 1501);
        assertEquals(0, result);
    }

    @Test
    public void price1500() {
        double result = sut.shippingCosts(true, IN_STORE, 1500);
        assertEquals(50, result);
    }

    @Test
    public void priceGround() {
        double result = sut.shippingCosts(true, GROUND, 1450);
        assertEquals(100, result);
    }

    @Test
    public void priceInStore() {
        double result = sut.shippingCosts(true, IN_STORE, 1000);
        assertEquals(50, result);
    }

    @Test
    public void priceNextDayAir() {
        double result = sut.shippingCosts(true, NEXT_DAY_AIR, 1000);
        assertEquals(250, result);
    }

    @Test
    public void priceSecondDayAir() {
        double result = sut.shippingCosts(true, SECOND_DAY_AIR, 1000);
        assertEquals(125, result);
    }

    @Test
    public void priceMethodUnknown() {
        double result = sut.shippingCosts(true, UNKNOWN, 1000);
        assertEquals(0, result);
    }

    @Test
    public void priceInvalid() {
        double result = sut.shippingCosts(true, GROUND, -1000);
        assertEquals(100, result);
    }
}
