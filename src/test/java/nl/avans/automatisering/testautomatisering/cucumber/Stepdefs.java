package nl.avans.automatisering.testautomatisering.cucumber;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Stepdefs {
    private static final String TEST_EMAIL = "avans@testing.nl";

    private static final int RETRY_WAIT_MS = 500;
    private static final boolean HEADLESS_MODE = true;

    private RemoteWebDriver driver;

    private void setDriver() {
//        System.setProperty("webdriver.gecko.driver", "Path to your driver");
        String webdriver = System.getProperty("browser", "firefox");
        System.out.println(webdriver);
        switch (webdriver) {
            case "firefox":
                setDriverFirefox();
                break;
            case "chrome":
                setDriverChrome();
                break;
            default:
                throw new IllegalStateException("unsupported driver: " + webdriver);
        }
    }

    private void setDriverFirefox() {
//      make sure your path is set correctly for Chrome AND Firefox
        log.info("using firefox");
        FirefoxOptions options = new FirefoxOptions();
        options.setHeadless(HEADLESS_MODE);
        driver = new FirefoxDriver(options);
    }

    private void setDriverChrome() {
        log.info("using chrome");
//      make sure your path is set correctly for Chrome AND Firefox
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(HEADLESS_MODE);
        options.addArguments("--disable-dev-shm-usage");
        driver = new ChromeDriver(options);
    }

    @Before
    public void setup() {
        productOptions = Arrays.asList(
            new Product("smartphone", 100.0),
            new Product("used phone", 5.0),
            new Product("blue jeans", 1.0),
            new Product("golf belt", 1.0)
        );
        r = new Random();
        cart = new ArrayList<>();
    }

    private List<Product> productOptions;
    private List<Product> cart;
    private Random r;

    @Given("^I am on the demo webshop on \"([^\"]*)\"$")
    public void iAmOnTheDemoWebshopOn(String browser) {
        switch (browser) {
            case "Firefox":
                setDriverFirefox();
                break;
            case "Chrome":
                setDriverChrome();
                break;
            default:
                throw new IllegalStateException("browser " + browser + " not supported");
        }
        driver.get("http://demowebshop.tricentis.com/");
    }

    @When("^I log in to the webshop$")
    public void iLogInToTheWebshop() {
        log.info("logging into the webshop");
        // press Log in to go to form
        clickElementByLinkText("Log in");
        // get email field and fill
        WebElement email = driver.findElement(By.id("Email"));
        email.clear();
        email.sendKeys(TEST_EMAIL);
        // get password field and fill
        WebElement password = driver.findElement(By.id("Password"));
        password.clear();
        password.sendKeys("t3st1ng?");
        // submit form
        driver.findElement(By.className("login-button")).click();
    }

    @And("^I am logged in correctly$")
    public void iAmLoggedInCorrectly() {
        assertTrue(driver.findElement(By.linkText(TEST_EMAIL)).isDisplayed());
    }

    @And("^I add x smartphones or used phones$")
    public void iAddBetweenSmartphonesOrUsedPhones() {
        log.info("adding smartphones or used phones");
        clickElementByLinkText("Electronics");
        clickElementByLinkText("Cell phones");
        int type = r.nextInt(2);
        int amount = r.nextInt(8) + 2;
        if (type == 0) {
            clickElementByLinkText("Smartphone");
            addProductsToCart(productOptions.get(0), amount, 43);
        } else if (type == 1) {
            clickElementByLinkText("Used phone");
            addProductsToCart(productOptions.get(1), amount, 15);
        }
    }

    @And("^I add x golf belts or blue jeans$")
    public void iAddXGolfBeltsOrBlueJeans() {
        clickElementByLinkText("Apparel & Shoes");
        int type = r.nextInt(2);
        int amount = r.nextInt(8) + 2;
        if (type == 0) {
            clickElementByLinkText("Blue Jeans");
            addProductsToCart(productOptions.get(2), amount, 36);
        } else if (type == 1) {
            clickElementByLinkText("Casual Golf Belt");
            addProductsToCart(productOptions.get(3), amount, 40);
        }
    }

    @Then("^the price in my shopping cart is correct$")
    public void thePriceInMyShoppingCartIsCorrect() {
        double total = calculateTotal();
        clickElementByLinkText("Shopping cart");
        WebElement subtotal = driver.findElement(By.className("product-price"));
        double shopTotal = Double.parseDouble(subtotal.getText());
        assertEquals(total, shopTotal, 0);
    }

    @And("^logout and close$")
    public void logoutAndClose() {
        cleanupCart();
        clickElementByLinkText("Log out");
        assertTrue(driver.findElement(By.linkText("Log in")).isDisplayed());
    }

    @After()
    public void cleanup(Scenario scenario) {
        if (driver != null) {
            if (scenario.isFailed()) {
                cleanupCart();
                byte[] screenshot = driver.getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            }
            driver.quit();
        }
    }

    private void cleanupCart() {
        clickElementByLinkText("Shopping cart");
        List<WebElement> elements = driver.findElements(By.className("qty-input"));
        for (WebElement element : elements) {
            element.clear();
            element.sendKeys("0");
        }
        driver.findElement(By.className("update-cart-button")).click();
    }

    private void clickElementByLinkText(String text) {
        try {
            log.info("click element by link text '{}'", text);
            driver.findElement(By.linkText(text)).click();
        } catch (NoSuchElementException ex) {
            clickElementByLinkTextRetry(text, 1);
        }
    }

    private void clickElementByLinkTextRetry(String text, int retry) {
        try {
            log.info("clickElementByLinkTextRetry: '{}'", text);
            retry++;
            driver.findElement(By.linkText(text)).click();
        } catch (NoSuchElementException ex) {
            log.info("waiting for {} millisecond(s) before retrying clickElementByLinkTextRetry: {}", RETRY_WAIT_MS, retry);
            if (retry > 5) {
                log.warn("retry exceeded 5 for text '{}'", text);
                throw new NoSuchElementException(text);
            }
            clickElementByLinkTextRetry(text, retry);
        }
    }

    private void addProductsToCart(Product product, int amount, int id) {
        log.info("adding {} of {}(s)", amount, product.getName());
        WebElement quantity = getElementRetry("addtocart_" + id + "_EnteredQuantity", 0);
        quantity.clear();
        quantity.sendKeys("" + amount);
        driver.findElement(By.id("add-to-cart-button-" + id)).click();
        for (int i = 0; i < amount; i++) {
            cart.add(product);
        }
    }

    private WebElement getElementRetry(String id, int retry) {
        log.info("getting element with id '{}'", id);
        WebElement element;
        if ((retry < 5)) {
            try {
                retry++;
                element = driver.findElement(By.id(id));
                return element;
            } catch (NoSuchElementException ex) {
                log.info("waiting for {} millisecond(s) before retrying getElementRetry: {}", RETRY_WAIT_MS, retry);
                sleep();
                getElementRetry(id, retry);
            }
        }
        log.warn("retry exceeded 5 for id '{}'", id);
        throw new NoSuchElementException(id);
    }

    private double calculateTotal() {
        double total = 0;
        for (Product product : cart) {
            total += product.getPrice();
        }
        return total;
    }

    private void sleep() {
        try {
            driver.wait(RETRY_WAIT_MS);
        } catch (InterruptedException ex) {
            log.warn("interrupted while waiting for retry");
        }
    }
}
