package nl.avans.automatisering.testautomatisering.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = "src/test/resources/cucumber",
    tags = "@Webshop",
    format = {"pretty", "html:target/cucumber", "json:target/cucumber.json"}
)
public class CucumberTest {
//    please note, that you need a web driver to run this.
//    either geckodriver (firefox) https://github.com/mozilla/geckodriver/releases
//    or chromdriver https://sites.google.com/a/chromium.org/chromedriver/
//    you can uncomment some lines in the Stepdefs to set the path, or add the driver(s) to your path.
}
