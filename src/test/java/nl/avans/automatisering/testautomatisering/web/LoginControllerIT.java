package nl.avans.automatisering.testautomatisering.web;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import nl.avans.automatisering.testautomatisering.TestAutomatiseringApplication;
import nl.avans.automatisering.testautomatisering.domain.LoginUser;
import nl.avans.automatisering.testautomatisering.service.LoginService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestAutomatiseringApplication.class, webEnvironment = RANDOM_PORT, value = {
    "spring.profiles.active=test"
})
public class LoginControllerIT {
    private static final String LOGIN_RESOURCE = "/login";
    @LocalServerPort
    private int port;

    @Autowired
    private LoginService loginService;

    @Before
    public void setUp() {
        RestAssured.port = port;
        RestAssured.basePath = "/test";

        loginService.setUsers(Collections.singletonList(new LoginUser("testUser", "t3st1ng?")));
    }

    @Test
    public void loginBadUsername() {
        given()
            .accept(JSON)
            .contentType(JSON)
            .body(new LoginUser("unknown", "t3st1ng?"))
            .post(LOGIN_RESOURCE).peek()
            .then()
            .assertThat()
            .statusCode(SC_BAD_REQUEST)
            .body("success", is(false))
            .body("username", containsString("unknown"))
            .body("message", containsString("login failed"));
    }

    @Test
    public void loginBadPassword() {
        given()
            .accept(JSON)
            .contentType(JSON)
            .body(new LoginUser("testUser", "unknown"))
            .post(LOGIN_RESOURCE).peek()
            .then()
            .assertThat()
            .statusCode(SC_BAD_REQUEST)
            .body("success", is(false))
            .body("message", containsString("passwords do not match"));
    }

    @Test
    public void loginValid() {
        given()
            .accept(JSON)
            .contentType(JSON)
            .body(new LoginUser("testUser", "t3st1ng?"))
            .post(LOGIN_RESOURCE).peek()
            .then()
            .assertThat()
            .statusCode(SC_OK)
            .body("success", is(true))
            .body("username", containsString("testUser"))
            .body("message", containsString("login successful"));
    }

    @Test
    public void loginInvalidPassword() {
        given()
            .accept(JSON)
            .contentType(JSON)
            .body(new LoginUser("testUser", "abc"))
            .post(LOGIN_RESOURCE).peek()
            .then()
            .assertThat()
            .statusCode(SC_BAD_REQUEST)
            .body("success", is(false))
            .body("password", containsString("password must be between 6 and 200 characters"));
    }

    @Test
    public void loginInvalidUsername() {
        given()
            .accept(JSON)
            .contentType(JSON)
            .body(new LoginUser("a", "t3st1ng?"))
            .post(LOGIN_RESOURCE).peek()
            .then()
            .assertThat()
            .statusCode(SC_BAD_REQUEST)
            .body("success", is(false))
            .body("username", containsString("username must be between 2 and 20 characters"));
    }

    @Test
    public void loginNoBody() {
        given()
            .accept(JSON)
            .contentType(JSON)
            .post(LOGIN_RESOURCE).peek()
            .then()
            .assertThat()
            .statusCode(SC_BAD_REQUEST);
    }
    
    @Test
    public void loginInvalidBody() {
        Map<String, String> request = new HashMap<>();
        request.put("parameter", "unknown");

        given()
            .accept(JSON)
            .contentType(JSON)
            .body(request)
            .post(LOGIN_RESOURCE).peek()
            .then()
            .assertThat()
            .statusCode(SC_BAD_REQUEST)
            .body("password", containsString("must not be blank"))
            .body("username", containsString("must not be blank"))
            .body("success", is(false));
    }
}
