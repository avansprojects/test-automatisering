package testautomatisering

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration._

class OrderHappyFlow extends Simulation {

	val httpProtocol: HttpProtocolBuilder = http
		.baseUrl("http://demowebshop.tricentis.com")
		.inferHtmlResources(BlackList(""".*\.js""", """.*\.css""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.woff2""", """.*\.(t|o)tf""", """.*\.png""", """.*detectportal\.firefox\.com.*"""), WhiteList())
		.acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
		.acceptEncodingHeader("gzip, deflate")

	val accept_all: Map[String, String] = Map(
		"Accept" -> "*/*")

	val multipart_form_header: Map[String, String] = Map(
		"Content-Type" -> "multipart/form-data; boundary=---------------------------13743185957913350191599440801")

	val accept_all_form: Map[String, String] = Map(
		"Accept" -> "*/*",
		"Content-Type" -> "application/x-www-form-urlencoded; charset=UTF-8")

	val scn: ScenarioBuilder = scenario("OrderHappyFlow")
		.exec(http("to login page")
			.get("/login"))
		.pause(20)
		.exec(http("login")
			.post("/login")
			.formParam("Email", "avans@testing.nl")
			.formParam("Password", "t3st1ng?")
			.formParam("RememberMe", "false"))
		.pause(20)
		.exec(http("browse electronics")
			.get("/electronics"))
		.pause(20)
		.exec(http("select cell-phones")
			.get("/cell-phones"))
		.pause(20)
		.exec(http("add cellphone to cart")
			.post("/addproducttocart/catalog/15/1/1")
			.headers(accept_all))
		.pause(20)
		.exec(http("go to cart")
			.get("/cart"))
		.pause(20)
		.exec(http("start checkout")
			.post("/cart")
			.headers(multipart_form_header))
//			.body(RawFileBody("checkout_request.dat")))
		.pause(20)
		.exec(http("select country")
			.get("/country/getstatesbycountryid?countryId=1&addEmptyStateIfRequired=true&_=1560159310015")
			.headers(accept_all))
		.pause(20)
		.exec(http("saving payment details")
			.post("/checkout/OpcSaveBilling/")
			.headers(accept_all_form)
			.formParam("BillingNewAddress.Id", "0")
			.formParam("BillingNewAddress.FirstName", "Avans")
			.formParam("BillingNewAddress.LastName", "Testing")
			.formParam("BillingNewAddress.Email", "avans@testing.nl")
			.formParam("BillingNewAddress.Company", "")
			.formParam("BillingNewAddress.CountryId", "1")
			.formParam("BillingNewAddress.StateProvinceId", "7")
			.formParam("BillingNewAddress.City", "Phoenix")
			.formParam("BillingNewAddress.Address1", "A road")
			.formParam("BillingNewAddress.Address2", "")
			.formParam("BillingNewAddress.ZipPostalCode", "90210")
			.formParam("BillingNewAddress.PhoneNumber", "0612345678")
			.formParam("BillingNewAddress.FaxNumber", ""))
		.pause(20)
		.exec(http("save shipping details")
			.post("/checkout/OpcSaveShipping/")
			.headers(accept_all_form)
			.formParam("shipping_address_id", "1004098")
			.formParam("ShippingNewAddress.Id", "0")
			.formParam("ShippingNewAddress.FirstName", "Avans")
			.formParam("ShippingNewAddress.LastName", "Testing")
			.formParam("ShippingNewAddress.Email", "avans@testing.nl")
			.formParam("ShippingNewAddress.Company", "")
			.formParam("ShippingNewAddress.CountryId", "0")
			.formParam("ShippingNewAddress.StateProvinceId", "0")
			.formParam("ShippingNewAddress.City", "")
			.formParam("ShippingNewAddress.Address1", "")
			.formParam("ShippingNewAddress.Address2", "")
			.formParam("ShippingNewAddress.ZipPostalCode", "")
			.formParam("ShippingNewAddress.PhoneNumber", "")
			.formParam("ShippingNewAddress.FaxNumber", "")
			.formParam("PickUpInStore", "true")
			.formParam("PickUpInStore", "false"))
		.pause(20)
		.exec(http("save payment method")
			.post("/checkout/OpcSavePaymentMethod/")
			.headers(accept_all_form)
			.formParam("paymentmethod", "Payments.CheckMoneyOrder"))
		.pause(20)
		.exec(http("save payment info")
			.post("/checkout/OpcSavePaymentInfo/")
			.headers(accept_all))
		.pause(20)
		.exec(http("confirm order")
			.post("/checkout/OpcConfirmOrder/")
			.headers(accept_all)
			.resources(http("request_18")
			.get("/checkout/completed/")))
		.pause(20)
		.exec(http("completed")
			.get("/"))

	setUp(scn.inject(
		rampConcurrentUsers(1) to 5 during(20 seconds)
	)).protocols(httpProtocol)
		.assertions(
			global.responseTime.max.lte(500),
			global.failedRequests.count.lt(1)
		)
  	.maxDuration(2 minutes)
}
