package testautomatisering

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration._

class LoginHappyFlow extends Simulation {
  val httpProtocol: HttpProtocolBuilder = http
    .baseUrl("http://demowebshop.tricentis.com")
    .inferHtmlResources(BlackList(""".*\.js""", """.*\.css""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.woff2""", """.*\.(t|o)tf""", """.*\.png""", """.*detectportal\.firefox\.com.*"""), WhiteList())
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .acceptEncodingHeader("gzip, deflate")

  val scn: ScenarioBuilder = scenario("OrderHappyFlow")
    .exec(http("to login page")
      .get("/login"))
    .pause(20 second)
    .exec(http("login")
      .post("/login")
      .formParam("Email", "avans@testing.nl")
      .formParam("Password", "t3st1ng?")
      .formParam("RememberMe", "false"))
    .pause(20 second)

  setUp(
    scn.inject(
      rampConcurrentUsers(1) to 5 during(20 seconds),
      rampConcurrentUsers(1) to 5 during(20 seconds)
    )
  )
    .protocols(httpProtocol)
    .maxDuration(2 minutes)
    .assertions(
      global.responseTime.max.lte(400)
    )
}
