package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url ('/validate/even') {
            queryParameters {
                parameter("nr", 3)
            }
        }

    }
    response {
        status 200
        body("odd")
    }
}

