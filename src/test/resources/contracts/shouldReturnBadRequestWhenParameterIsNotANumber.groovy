package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url ('/validate/even') {
            queryParameters {
                parameter("nr", "two")
            }
        }

    }
    response {
        status 400
//        body("even")
    }
}

