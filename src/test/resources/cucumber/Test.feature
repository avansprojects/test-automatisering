Feature: Ordering random items should give us the right sub total

  @Webshop
  Scenario Outline: Order some clothing
    Given I am on the demo webshop on "<browser>"
    When I log in to the webshop
    And I am logged in correctly
    And I add x smartphones or used phones
    And I add x golf belts or blue jeans
    Then the price in my shopping cart is correct
    And logout and close


  Examples:
    | browser   |
    | Firefox |
    | Chrome  |
